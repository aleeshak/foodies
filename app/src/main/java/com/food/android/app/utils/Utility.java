package com.food.android.app.utils;
import android.content.Context;
import android.provider.Settings.Secure;

/**
 * Created by Aleesha Kanwal on 9/25/2019.
 */
public class Utility {

    public static String getDevceID(Context context){
        return  Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);
    }
}
