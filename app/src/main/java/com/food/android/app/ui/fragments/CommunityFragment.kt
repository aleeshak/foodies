package com.food.android.app.ui.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.food.android.app.R
import com.food.android.app.adapters.HomeRecyclerAdapter
import com.food.android.app.models.Item
import com.food.android.app.ui.activities.AddPostActivity
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_find.*


/**
 * Created by Aleesha Kanwal on 2/2/2020.
 */

class CommunityFragment : Fragment() {

    private lateinit var recyclerAdapter: HomeRecyclerAdapter
    private lateinit var gridLayoutManager: RecyclerView.LayoutManager

    internal lateinit var databaseReference: DatabaseReference

    internal lateinit var mDatabase: FirebaseDatabase

    internal lateinit var progressDialog: ProgressDialog

    internal var list: ArrayList<Item> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_find, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
        setupSpinner()
    }

    private fun setup() {

        progressDialog = ProgressDialog(context)

        mDatabase = FirebaseDatabase.getInstance()

        databaseReference = mDatabase.getReference(AddPostActivity.Database_Path)

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                list = ArrayList()

                if(snapshot.hasChildren()) {
                    for (dataSnapshot in snapshot.children) {
                        val studentDetails = dataSnapshot.getValue(Item::class.java)
                        list.add(studentDetails!!)
                    }

                    recyclerAdapter = HomeRecyclerAdapter(list, context!!)
                    gridLayoutManager = GridLayoutManager(context, 2)
                    recyclerview.layoutManager = gridLayoutManager
                    recyclerview.adapter = recyclerAdapter
                }

                progressDialog.dismiss()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                progressDialog.dismiss()

            }
        })
    }

    private fun setupSpinner() {

        val dataAdapter = ArrayAdapter(context, android.R.layout.simple_spinner_item,  resources.getStringArray(R.array.community))
        dataAdapter.setDropDownViewResource(R.layout.dropdown_menu_popup_item)
        spinner.adapter = dataAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                //spinner.selectedItemPosition
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
    }
}