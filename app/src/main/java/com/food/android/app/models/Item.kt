package com.food.android.app.models


/**
 * Created by Aleesha Kanwal on 3/14/2020.
 */
data class Item (val id : String, val name : String,val price : Long,val location : String, val description : String, val category: String, val community : String, val country : String, val image : String){
    constructor() : this("","",0,"","","","","","")
}