package com.food.android.app.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.food.android.app.R
import com.food.android.app.models.Item
import com.food.android.app.ui.activities.DetailActivity
import kotlinx.android.synthetic.main.recyclerview_item.view.*

class HomeRecyclerAdapter(val items: ArrayList<Item>, val context: Context) : RecyclerView.Adapter<HomeRecyclerAdapter.ViewHolder>() {

    var onClick: (view: View, position: Int) -> Unit = { _, _ -> }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.recyclerview_item, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bindView(position, onClick)

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val animalImage = view.animalImage
        val parentLayout = view.parentLayout
        val titleTv = view.titleTv
        val descriptionTv = view.descriptionTv



        fun bindView(position: Int, onClick: (view: View, position: Int) -> Unit) {

            val data = items.get(position)
           // animalImage.setImageResource(resId)
            itemView.setOnClickListener {
                onClick(itemView, position)
            }

            titleTv.setText(data.name)
            descriptionTv.setText(data.description)

            parentLayout.setOnClickListener {
                parentLayout.context.startActivity(Intent(parentLayout.context, DetailActivity::class.java))
            }
        }
    }

}

