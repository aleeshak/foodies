package com.food.android.app.ui.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import com.firebase.client.Firebase
import com.food.android.app.R
import com.food.android.app.models.Item
import com.food.android.app.ui.activities.base.BaseActivity
import com.food.android.app.utils.MatissePicassoEngine
import com.food.android.app.utils.MyHelper
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.cloud.FirebaseVisionCloudDetectorOptions
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.internal.entity.CaptureStrategy
import kotlinx.android.synthetic.main.activity_add_post.*
import java.util.*
import kotlin.collections.ArrayList

class AddPostActivity : BaseActivity() {

    val PERMISSIONS_REQUEST_READ_AND_WRITE_EXTERNAL_STORAGE = 123

    private val RC_CAMERA = 3000
    private var images: ArrayList<Image> = ArrayList()

    private val requestCodeChoose: Int = 1
    private var mSelected: List<Uri>? = null
    private var mBitmap: Bitmap? = null
    //private var mImageView: ImageView? = null
    var dataUri: Uri? = null

    //Firebase
    lateinit var storage: FirebaseStorage
    lateinit var storageReference: StorageReference

    companion object {

        // Declaring String variable ( In which we are storing firebase server URL ).
        val Firebase_Server_URL = "https://foodies-891d7.firebaseio.com/"

        // Root Database Name for Firebase Database.
        val Database_Path = "table_items"

        internal lateinit var firebase: Firebase

        internal lateinit var databaseReference: DatabaseReference

        internal lateinit var mDatabase: FirebaseDatabase
    }

    var id = ""

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)
        initToolbar()
        setupSpinnerCommunity()
        setupSpinnerCategories()
        setupSpinnerCountry()

        firebase = Firebase(Firebase_Server_URL)

        mDatabase = FirebaseDatabase.getInstance()

        databaseReference = mDatabase.getReference(Database_Path)

        storage = FirebaseStorage.getInstance()

        storageReference = storage.getReference()


        mImageView.setOnClickListener {
            //setUpGallery()
          //  getImagePicker()
            checkStoragePermission(RC_STORAGE_PERMS1)
        }

        //checkPermissionREAD_EXTERNAL_STORAGE(this)
    }

    private fun postItemToDatabase(image : String) {

        id =  UUID.randomUUID().toString()

        val name = name.getText().toString().trim({ it <= ' ' })

        val price = price.getText().toString().trim({ it <= ' ' })

        val location = location.getText().toString().trim({ it <= ' ' })

        val desc = desc.getText().toString().trim({ it <= ' ' })

        val item = Item(id , name, price.toLong(), location, desc, spinnerCategory.selectedItem.toString(), spinnerCommunity.selectedItem.toString(), spinnerCountry.selectedItem.toString(), image)

        // Getting the ID from firebase database.
        val StudentRecordIDFromServer = databaseReference.push().key

        // Adding the both name and number values using student details class object using ID.
        databaseReference.child(StudentRecordIDFromServer!!).setValue(item)

        // Showing Toast message after successfully data submit.
        Toast.makeText(this@AddPostActivity, "Item Posted Successfully", Toast.LENGTH_LONG).show()
    }

    fun setUpGallery() {
        Matisse.from(AddPostActivity@ this)
                .choose(MimeType.ofImage(), false)
                .countable(true)
                .capture(true)
                .captureStrategy(CaptureStrategy(true, "com.find.it.app.fileprovider", "Findit"))
                .maxSelectable(4)
                .restrictOrientation(SCREEN_ORIENTATION_PORTRAIT)
                .imageEngine(MatissePicassoEngine())
                .showSingleMediaType(true)
                .theme(R.style.AppImageSelectorTheme)
                .autoHideToolbarOnSingleTap(true)
                .forResult(requestCodeChoose)
    }
/*
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == requestCodeChoose && resultCode == RESULT_OK) {
            mSelected = Matisse.obtainResult(data);
            Log.d("Matisse", "mSelected: " + mSelected);
        }

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            this.images.addAll(ImagePicker.getImages(data))
        }
    }*/


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                RC_STORAGE_PERMS1 -> checkStoragePermission(requestCode)
                RC_SELECT_PICTURE -> {
                    dataUri = data?.data
                    val path = MyHelper.getPath(this, dataUri)
                    if (path == null) {
                        mBitmap = MyHelper.resizeImage(imageFile, this, dataUri, mImageView)
                    } else {
                        mBitmap = MyHelper.resizeImage(imageFile, path, mImageView)
                    }
                    if (mBitmap != null) {
                        //mTextView.setText(null)
                        mImageView?.setImageBitmap(mBitmap)
                    }
                }
                RC_CAMERA -> {
                    captureImage()
                    /*if (grantResults.size != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        captureImage()
                    }*/
                }
            }
        }
    }


    fun checkPermissionREAD_EXTERNAL_STORAGE(
            context: Context): Boolean {
        val currentAPIVersion = Build.VERSION.SDK_INT
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                                context as Activity,
                                Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context,
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE))

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    context,
                                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                    PERMISSIONS_REQUEST_READ_AND_WRITE_EXTERNAL_STORAGE)
                }
                return false
            } else {
                return true
            }

        } else {
            return true
        }
    }

    fun showDialog(msg: String, context: Context,
                   permissions: Array<String>) {
        val alertBuilder = AlertDialog.Builder(context)
        alertBuilder.setCancelable(true)
        alertBuilder.setTitle("Permission necessary")
        alertBuilder.setMessage("$msg permission is necessary")
        alertBuilder.setPositiveButton(android.R.string.yes,
                DialogInterface.OnClickListener { dialog, which ->
                    ActivityCompat.requestPermissions(context as Activity,
                            permissions,
                            PERMISSIONS_REQUEST_READ_AND_WRITE_EXTERNAL_STORAGE)
                })
        val alert = alertBuilder.create()
        alert.show()
    }

    private fun captureImage() {
        ImagePicker.cameraOnly().start(this)
    }

/*
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
//            PERMISSIONS_REQUEST_READ_AND_WRITE_EXTERNAL_STORAGE -> if (grantResults[0] === PackageManager.PERMISSION_GRANTED) {
//                // do your stuff
//            } else {
//                Toast.makeText(this, "GET_ACCOUNTS Denied",
//                    Toast.LENGTH_SHORT).show()
//            }
//            else -> super.onRequestPermissionsResult(requestCode, permissions,
//                grantResults)
             RC_CAMERA -> {
                if (grantResults.size != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    captureImage()
                }
            }

        }

    }
*/


    private fun getImagePicker() {

        ImagePicker.create(this)
                .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Gallery") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                .includeVideo(false) // Show video on image picker
                .single() // multiple mode
                .origin(images) // original selected images, used in multi mode
                .limit(4) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                .theme(R.style.ImagePickerTheme) // must inherit ef_BaseTheme. please refer to sample
                .enableLog(false) // disabling log
                .start()
    }


    private fun setupSpinnerCategories() {

        val dataAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.categories))
        dataAdapter.setDropDownViewResource(R.layout.dropdown_menu_popup_item)
        spinnerCategory.adapter = dataAdapter

        spinnerCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                //spinnerCommunity.selectedItemPosition
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
    }

    private fun setupSpinnerCommunity() {

        val dataAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.community))
        dataAdapter.setDropDownViewResource(R.layout.dropdown_menu_popup_item)
        spinnerCommunity.adapter = dataAdapter

        spinnerCommunity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                //spinnerCommunity.selectedItemPosition
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
    }

    private fun setupSpinnerCountry() {

        val dataAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.country))
        dataAdapter.setDropDownViewResource(R.layout.dropdown_menu_popup_item)
        spinnerCountry.adapter = dataAdapter

        spinnerCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                //spinnerCountry.selectedItemPosition
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.my_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {

                uploadImage()
                validatePhoto()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun validatePhoto() {
        if (mBitmap != null) {
            MyHelper.showDialog(this)
            val options = FirebaseVisionCloudDetectorOptions.Builder()
                    .setModelType(FirebaseVisionCloudDetectorOptions.LATEST_MODEL)
                    .setMaxResults(5)
                    .build()

            val image = FirebaseVisionImage.fromBitmap(mBitmap!!)
            val detector = FirebaseVision.getInstance().getVisionCloudLabelDetector(options)
            detector.detectInImage(image).addOnSuccessListener { labels ->
                MyHelper.dismissDialog()
                for (label in labels) {
                    mTextView.append(label.label + ": " + label.confidence + "\n")
                    mTextView.append(label.entityId + "\n")
                }
            }.addOnFailureListener { e ->
                MyHelper.dismissDialog()
                mTextView.text = e.message
            }
        }
    }

    fun uploadImage() {
        storageReference = storageReference.child("images/" + id)
        storageReference.putFile(dataUri!!).addOnSuccessListener {

            storageReference.downloadUrl.addOnSuccessListener {
                postItemToDatabase(it.toString())
            }

            postItemToDatabase(it.metadata?.reference?.downloadUrl.toString())


            Log.d("main", "Your photo is uploaded successfully")
        }
    }
}