package com.food.android.app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import com.food.android.app.R
import com.food.android.app.ui.activities.base.BaseActivity
import com.smarteist.autoimageslider.DefaultSliderView
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import kotlinx.android.synthetic.main.activity_detail.*


/**
 * Created by Aleesha Kanwal on 2/9/2020.
 */

class DetailActivity : BaseActivity() {

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        initToolbar()


        imageSlider.setIndicatorAnimation(IndicatorAnimations.SWAP); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        imageSlider.setSliderTransformAnimation(SliderAnimations.FADETRANSFORMATION);
        imageSlider.setScrollTimeInSec(5); //set scroll delay in seconds :

        initSlider()
    }

    fun initSlider(){

        for (i in 0..3) {

            val sliderView = DefaultSliderView(this)

            when (i) {
                0 -> sliderView.setImageDrawable(R.drawable.ic_launcher_background)
                1 -> sliderView.imageUrl = "https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                2 -> sliderView.imageUrl = "https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260"
                3 -> sliderView.imageUrl = "https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
            }

            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP)
            sliderView.description = ""

            sliderView.setOnSliderClickListener { Toast.makeText(this@DetailActivity, "This is slider " + (i + 1), Toast.LENGTH_SHORT).show() }

            //at last add this view in your layout :
            imageSlider.addSliderView(sliderView)
        }
    }

}
